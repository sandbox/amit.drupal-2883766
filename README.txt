CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Author/Maintainers

INRODUCTION
-----------

 * It provides a beautiful progress indicator for page scroll.

REQUIREMENTS
------------

 * None.

INSTALLATION
------------

 * To install, copy the headbar directory and all its 
   contents to your sites/all/modules
  directory.

 * To enable the module go to Administer > Modules, 
   and enable "page scroll Indicator".

CONFIGURATION
-------------
 * Go To admin/config/user-interface/page-scroll-indicator

 * Select Option for Scroll to use, and Color of scroll.


AUTHOR/MAINTAINERS
------------------
Author:
 * Amit Kumar(amit.drupal) - https://www.drupal.org/user/1622130


Current maintainers:
 * Vidushi Mehta(vidushi-mehta) - https://www.drupal.org/user/3088465
