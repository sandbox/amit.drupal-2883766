<?php

namespace Drupal\page_scroll_indicator\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Configure page_scroll_indicator settings.
 */
class PageScrollIndicatorConfigureForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'page_scroll_indicator_configure_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['page_scroll_indicator.configure'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $page_scroll_indicator_config = $this->config('page_scroll_indicator.configure');

    $form['scroll_option'] = [
      '#title' => $this->t('Select Option for Scroll to use'),
      '#description' => $this->t('Scroll comes with a lot of option for progress.'),
      '#type' => 'radios',
      '#options' => [
        '1' => $this->t('Straight line'),
        '2' => $this->t('Circular progress'),
        '3' => $this->t('Animated progress'),
        '4' => $this->t('Tooltip progress'),
        '5' => $this->t('Bottom line'),
        '6' => $this->t('No Scroll'),
      ],
      '#default_value' => $page_scroll_indicator_config->get('scroll_option'),
    ];
    $form['scroll_progress_color'] = [
      '#title' => $this->t('Color code.'),
      '#description' => $this->t('Default color for scroll progress is #ff0000.'),
      '#type' => 'textfield',
      '#default_value' => $page_scroll_indicator_config->get('scroll_progress_color'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('page_scroll_indicator.configure')
      ->set('scroll_option', $form_state->getValue('scroll_option'))
      ->set('scroll_progress_color', $form_state->getValue('scroll_progress_color'))
      ->set('scroll_for_admin', $form_state->getValue('scroll_for_admin'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
