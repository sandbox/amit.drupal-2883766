(function (event) {
  'use strict';
  Drupal.behaviors.circular_progress_js = {
    attach: function (context, settings) {
      var scroll_progress_color = drupalSettings.page_scroll_indicator.scroll_progress_color;
      var body = document.getElementsByTagName('body')[0];
      body.innerHTML += '<div class="circular-progress-indicator" id="indicator"><svg><g><circle cx="0" cy="0" r="18" stroke="black" class="animated-circle" id="circle" transform="translate(50,50) rotate(-90)"/></g><g><circle cx="0" cy="0" r="28" transform="translate(50,50) rotate(-90)"/></g></svg><div class="circular-progress-count" id="count">0%</div></div>';
      var classFirst = document.getElementById('count');
      var classSecond = document.getElementById('circle');
      classFirst.setAttribute('style', 'color:' + scroll_progress_color);
      classSecond.setAttribute('style', 'stroke:' + scroll_progress_color);
      // scroll event function
      window.addEventListener('scroll', function (e) {
        var perc = document.scrollingElement.scrollTop / (document.body.scrollHeight - window.innerHeight);
        var element = document.querySelector('.circular-progress-indicator');
        if (perc > 0) {
          fadeIn(element, 'inline-block');
        }
        else {
          fadeOut(element);
        }
        updateProgress(perc);
      });

      function updateProgress(perc) {
        var circle_offset = 114 * perc;
        classSecond.style.strokeDashoffset = 126 - circle_offset;
        classFirst.innerHTML = document.write = (Math.round(perc * 100) + '%');
      }
    }
  };
}('scroll'));
