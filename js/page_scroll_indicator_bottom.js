(function (event) {
  'use strict';
  Drupal.behaviors.scrolljs = {
    attach: function (context, settings) {
      var scroll_progress_color = drupalSettings.page_scroll_indicator.scroll_progress_color;
      var body = document.getElementsByTagName('body')[0];
      body.innerHTML += ('<div class="scroll-progress-bottom" id="progress-bottom"><div class="bottom-progress-tooltip" id="progress-tooltip">0</div><span class="scroll-progress-bottom-triangle" id="progress-triangle"></span></div>');
      // scroll event function
      window.addEventListener('scroll', function (e) {
        var scrollPercent = 100 * document.scrollingElement.scrollTop / (document.body.scrollHeight - window.innerHeight);
        var classFirst = document.getElementById('progress-bottom');
        var classSecond = document.getElementById('progress-tooltip');
        var classThird = document.getElementById('progress-triangle');

        var styleFirst = 'width: ' + scrollPercent + '%; background-color: ' + scroll_progress_color + '; box-shadow: 0 0 8px ' + scroll_progress_color + ';';
        var styleSecond = 'background-color: ' + scroll_progress_color + ';';
        var styleThird = 'border-top: 11px solid ' + scroll_progress_color + ';';

        classFirst.setAttribute('style', styleFirst);
        classSecond.setAttribute('style', styleSecond);
        classThird.setAttribute('style', styleThird);

        classSecond.innerHTML = document.write = (Math.round(scrollPercent) + '%');

        var element = document.querySelector('.scroll-progress-bottom');
        if (scrollPercent > 0) {
          fadeIn(element, 'inline-block');
        }
        else {
          fadeOut(element);
        }
      });
    }
  };
}('scroll'));
