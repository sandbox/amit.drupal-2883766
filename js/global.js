// Global Functions
// function for fadeIn effect

function fadeIn(el, display) {
  'use strict';
  el.style.opacity = 0;
  el.style.display = display || 'block';

  (function fade() {
    var val = parseFloat(el.style.opacity);
    if (!((val += .1) > 1)) {
      el.style.opacity = val;
      requestAnimationFrame(fade);
    }
  })();
}

// function for fadeOut effect
function fadeOut(el) {
  'use strict';
  el.style.opacity = 1;

  (function fade() {
    if ((el.style.opacity -= .1) < 0) {
      el.style.display = 'none';
    }
    else {
      requestAnimationFrame(fade);
    }
  })();
}
