(function (event) {
  'use strict';
  Drupal.behaviors.scrolljs = {
    attach: function (context, settings) {
      var scroll_progress_color = drupalSettings.page_scroll_indicator.scroll_progress_color;
      var bar = document.createElement('div');
      var body = document.getElementsByTagName('body')[0];
      body.appendChild(bar);
      bar.setAttribute('class', 'bar-long');
      // scroll event function
      window.addEventListener('scroll', function (e) {
        var scrollPercent = 100 * document.scrollingElement.scrollTop / (document.body.scrollHeight - window.innerHeight);
        var style = 'width:' + scrollPercent + '%; background-color: ' + scroll_progress_color + '; box-shadow: 0 0 8px ' + scroll_progress_color + ';';
        bar.setAttribute('style', style);
      });
    }
  };
}('scroll'));
